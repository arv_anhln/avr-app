import {StyleSheet} from 'react-native';
import {getHeight, getWidth, normalize} from '@common/index';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  background: {
    flex: 1,
  },
  logo: {
    width: getWidth(146),
    height: getHeight(66),
    resizeMode: 'contain',
  },
  contentCenter: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
