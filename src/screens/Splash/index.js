import React, {Component} from 'react';
import {View, Image, ImageBackground} from 'react-native';
import {Style} from '@common/index';
import {Images} from '@config';
import {styles} from './style';

export default class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          source={Images.background}
          style={[styles.background, {position: 'relative'}]}>
          <View style={[styles.contentCenter, {flex: 1}]}>
            <Image style={styles.logo} source={Images.ic_logo} />
          </View>
        </ImageBackground>
      </View>
    );
  }
}
