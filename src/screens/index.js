import Splash from './Splash';
import Login from './Login';
import Registration from './Registration';

export {Splash, Login, Registration};
