import {StyleSheet} from 'react-native';
import {
  fontFamily,
  ScreenHeight,
  ScreenWidth,
  getHeight,
  getWidth,
} from '@common/index';

const styles = StyleSheet.create({
  backgroundLogin: {
    width: ScreenWidth,
    height: ScreenHeight,
    resizeMode: 'contain',
    position: 'relative',
  },
  loginLogo: {
    width: getHeight(89),
    height: getHeight(89),
    resizeMode: 'contain',
    position: 'absolute',
    top: getHeight(88),
    left: ScreenWidth / 2 - getHeight(89) / 2,
  },
  // login form
  loginForm: {
    paddingHorizontal: getWidth(20),
    paddingVertical: getWidth(30),
    marginHorizontal: getHeight(15),
    backgroundColor: '#fff',
    borderRadius: getHeight(40),
    alignItems: 'center',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: getHeight(120),
    opacity: 1,
    borderColor: '#ffbf29',
    borderWidth: getHeight(1),
  },
  loginGroupInput: {
    width: '100%',
  },
  loginInput: {
    paddingLeft: getHeight(5),
    fontWeight: '900',
  },
  // bottom button
  btnLogin: {
    backgroundColor: '#0023c4',
    paddingVertical: getHeight(20),
    alignSelf: 'stretch',
    borderRadius: getHeight(30),
    marginBottom: getHeight(10),
  },
  disableBtnLogin: {
    backgroundColor: 'rgba(0,35,196,0.7)',
  },
  buttonText: {
    color: '#fff',
    fontSize: getHeight(13.2),
    fontFamily: fontFamily.f3,
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  loading: {
    position: 'absolute',
    right: '50%',
    transform: [{translateX: getHeight(70)}],
  },
  flexD: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
});

export default styles;
