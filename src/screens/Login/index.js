import React, {Component} from 'react';
import {
  Animated,
  Text,
  StatusBar,
  ImageBackground,
  TouchableOpacity,
  Keyboard,
  Platform,
  ActivityIndicator,
  View,
} from 'react-native';
import {Style, validateForm, setMultiStorage, getHeight} from '@common/index';
import {Images, Config} from '@config/index';
import styles from './style';
import {auth} from '@services/index';
import Toast from 'react-native-simple-toast';
import {InputCustom} from '@components/index';
import {Base} from '@stores/index';
const HEADER_MAX_HEIGHT = -getHeight(190);
const HEADER_MIN_HEIGHT = 0;
export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPass: true,
      fields: {username: '', password: ''},
      errors: {},
      disabled: true,
      positionForm: new Animated.Value(0),
      isLoading: false,
    };
    this.devMode = 0;
    this._keyboardDidShow = this._keyboardDidShow.bind(this);
    this._keyboardDidHide = this._keyboardDidHide.bind(this);
  }

  componentDidMount() {
    this.init();
    if (Platform.OS === 'ios') {
      this.keyboardDidShowListener = Keyboard.addListener(
        'keyboardWillShow',
        this._keyboardDidShow,
      );
      this.keyboardDidHideListener = Keyboard.addListener(
        'keyboardWillHide',
        this._keyboardDidHide,
      );
    } else {
      this.keyboardDidShowListener = Keyboard.addListener(
        'keyboardDidShow',
        this._keyboardDidShow,
      );
      this.keyboardDidHideListener = Keyboard.addListener(
        'keyboardDidHide',
        this._keyboardDidHide,
      );
    }
  }

  init = async () => {
    let {fields} = this.state;
    if (fields.username !== null && fields.password !== null) {
      this.setState({disabled: false});
    } else {
      this.setState({disabled: true});
    }
  };

  onChangeText(field, value) {
    let list = this.state.fields;
    list[field] = value;
    this.setState({fields: list}, () => {
      if (list.username !== null && list.password !== null) {
        this.setState({disabled: false});
      } else {
        this.setState({disabled: true});
      }
    });
    return;
  }

  onShowPass = () => {
    this.setState({showPass: !this.state.showPass});
  };

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow() {
    Animated.timing(this.state.positionForm, {
      toValue: HEADER_MAX_HEIGHT,
      duration: 250,
      useNativeDriver: true,
    }).start();
  }

  _keyboardDidHide() {
    Animated.timing(this.state.positionForm, {
      toValue: HEADER_MIN_HEIGHT,
      duration: 300,
      useNativeDriver: true,
    }).start();
  }

  render() {
    let {errors, fields, positionForm} = this.state;
    const logoScale = positionForm.interpolate({
      inputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
      outputRange: [0.8, 1],
      extrapolate: 'clamp',
    });

    return (
      <TouchableOpacity
        style={Style.container}
        onPress={() => {
          Keyboard.dismiss();
        }}
        activeOpacity={1}>
        <ImageBackground
          style={styles.backgroundLogin}
          source={Images.background}>
          <StatusBar
            barStyle={'light-content'}
            translucent={true}
            backgroundColor={'transparent'}
          />
          <Animated.Text
            style={[styles.loginLogo, {transform: [{scale: logoScale}]}]}>
            AVR
          </Animated.Text>
          <Animated.View
            style={[
              styles.loginForm,
              {transform: [{translateY: positionForm}]},
            ]}>
            <InputCustom
              appendType={'icon'}
              iconAppend={Images.ic_person_login}
              onChangeText={(value) => this.onChangeText('username', value)}
              error={errors.username}
              value={fields.username}
              placeholder={'Tên đăng nhập'}
              autoCapitalize={'none'}
              placeholderTextColor={'rgba(0,0,1,0.7)'}
              styleGroup={styles.loginGroupInput}
              styleInput={styles.loginInput}
            />
            <InputCustom
              appendType={'icon'}
              iconAppend={
                this.state.showPass ? Images.ic_eye : Images.ic_eye_hide
              }
              onAppendPress={this.onShowPass}
              onChangeText={(value) => this.onChangeText('password', value)}
              error={errors.password}
              value={fields.password}
              placeholder={'Mật khẩu'}
              autoCapitalize={'none'}
              placeholderTextColor={'rgba(0,0,1,0.7)'}
              secureTextEntry={this.state.showPass}
              styleGroup={styles.loginGroupInput}
              styleInput={styles.loginInput}
              auto
              selectionColor={'#545c5f'}
            />
            <TouchableOpacity
              style={[
                styles.btnLogin,
                this.state.disabled ? styles.disableBtnLogin : null,
              ]}
              activeOpacity={0.7}
              onPress={() => {
                this.setState({
                  isLoading: true,
                });
              }}
              disabled={this.state.disabled || this.state.isLoading}>
              <View style={styles.flexD}>
                <Text style={styles.buttonText}>đăng nhập</Text>
                {this.state.isLoading && (
                  <View style={styles.loading}>
                    <ActivityIndicator size="small" color="#fff" />
                  </View>
                )}
              </View>
            </TouchableOpacity>
          </Animated.View>
        </ImageBackground>
      </TouchableOpacity>
    );
  }
}
