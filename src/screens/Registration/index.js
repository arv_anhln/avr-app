/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  RefreshControl,
} from "react-native";
import { ImageBackground, Header } from "@components/index";
import { styles } from "./style";
import { Images } from "@common/index";
import { InputCustom } from "@components/index";

export function Registration() {
  const [showPass, setShowPass] = useState(true);
  const onShowPass = () => {
    setShowPass(!showPass);
  };
  return (
    <ImageBackground>
      <View style={{ flex: 1 }}>
        <Header left="back" />
        <View style={styles.container}>
          <View>
            <Text style={styles.textHeader}>Create an account</Text>
            <View style={styles.flexD}>
              <Text style={styles.textAlready}>Already have an account?</Text>
              <TouchableOpacity>
                <Text style={styles.textLogin}>Login</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.viewInput}>
            <InputCustom
              appendType={"icon"}
              iconAppend={Images.ic_mail}
              placeholder={"Email"}
              autoCapitalize={"none"}
              placeholderTextColor={"rgba(0,0,1,0.7)"}
              styleGroup={styles.loginGroupInput}
              styleInput={styles.loginInput}
            />
            <InputCustom
              appendType={"icon"}
              iconAppend={showPass ? Images.ic_password : Images.ic_mail}
              onAppendPress={onShowPass}
              placeholder={"Password"}
              autoCapitalize={"none"}
              placeholderTextColor={"rgba(0,0,1,0.7)"}
              secureTextEntry={showPass}
              styleGroup={styles.loginGroupInput}
              styleInput={styles.loginInput}
              auto
            />
          </View>
          <Text style={styles.textDes}>
          By clicking Create account, I agree that I have read and accepted the
           <TouchableOpacity>
             <Text style={styles.textColorOrange}>Term of Use</Text>
             </TouchableOpacity>
            and Privacy Policy.
          </Text>
        </View>
      </View>
    </ImageBackground>
  );
}

export default Registration;
