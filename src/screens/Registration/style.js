import { StyleSheet } from "react-native";
import { fontFamily } from "@common/Style";
import { getHeight, getWidth, ScreenWidth } from "@common/Helper";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: getHeight(15),
    paddingHorizontal: getHeight(15),
    backgroundColor: "#fde7e1",
    borderTopLeftRadius: getHeight(25),
    borderTopRightRadius: getHeight(25),
    marginTop: getHeight(20),
  },
  flexD: {
    flexDirection: "row",
  },
  textHeader: {
    fontSize: getHeight(36.15),
    paddingTop: getHeight(30),
    fontWeight: "bold",
  },
  textAlready: {
    fontSize: getHeight(20.67),
    color: "#121212",
  },
  textLogin: {
    color: "#ff5400",
    fontSize: getHeight(20.67),
    paddingLeft: getWidth(5),
  },
  loginGroupInput: {
    width: "100%",
  },
  loginInput: {
    paddingLeft: getHeight(20),
    fontWeight: "900",
  },
  viewInput:{
    paddingTop:getHeight(40)
  },
  textDes:{
    fontSize:getHeight(18),
    color:'#282828'
  },
  textColorOrange:{
    color:'#ff5400'
  }
});
