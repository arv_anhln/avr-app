import React, {Component, useState, useEffect} from 'react';
import {Text, View, Image, TouchableOpacity, StatusBar} from 'react-native';
import styles from './style';
import {Images} from '@config/index';
import {useNavigation} from '@react-navigation/native';
import {observer} from 'mobx-react';
import {Notifications} from '@stores/index';
import {getWidth} from '@common/index';

export class Header extends Component {
  constructor(props) {
    super(props);
  }

  onLeftPress = () => {
    if (this.props.left === 'back' || this.props.left === 'close') {
      if (this.props.onLeftPress) {
        this.props.onLeftPress();
      } else {
        this.props.navigation.goBack();
      }
    } else if (this.props.left === 'arrowBack') {
      typeof this.props.onLeftPress !== 'undefined' && this.props.onLeftPress();
    } else {
      typeof this.props.onLeftPress !== 'undefined' && this.props.onLeftPress();
    }
  };

  onRightPress = () => {
    if (this.props.right === 'notify') {
      Notifications.setNotification(false);
      this.props.navigation.navigate('Notification');
    }
    typeof this.props.onRightPress !== 'undefined' && this.props.onRightPress();
  };

  renderLeft = () => {
    if (this.props.left === 'logo') {
      return <Image style={styles.ic_logo} source={Images.ic_logo_home} />;
    }
    if (this.props.left === 'back') {
      return <Image style={styles.back} source={Images.ic_back} />;
    }
    if (this.props.left === 'close') {
      return <Image style={styles.back} source={Images.ic_close} />;
    }
    if (this.props.left === 'arrowBack') {
      return <Image style={styles.back} source={Images.ic_close} />;
    }
    if (this.props.left === 'avatar') {
      return (
        <TouchableOpacity
          style={styles.avatar}
          onPress={() => this.props.navigation.navigate('EditProfile')}
          activeOpacity={1}>
          <View style={styles.avWrapImage}>
            <Image style={styles.avImage} source={this.props.avatar} />
          </View>
          <View style={styles.avContent}>
            <Text style={styles.avTitle}>{this.props.name}</Text>
            <Text style={styles.avSubTitle}>{this.props.class}</Text>
          </View>
        </TouchableOpacity>
      );
    }
  };

  renderCenter = () => {
    if (this.props.renderCenter) {
      return this.props.renderCenter;
    }
    if (this.props.center === 'sub') {
      return (
        <View>
          {this.props.subTitle && (
            <Text style={styles.subTitle}>{this.props.subTitle}</Text>
          )}
          {this.props.subMainTitle && (
            <Text numberOfLines={1} style={styles.subMainTitle}>
              {this.props.subMainTitle}
            </Text>
          )}
        </View>
      );
    }
    if (this.props.center === 'avatar') {
      return (
        <View style={styles.avatar}>
          <View style={styles.avWrapImage}>
            <Image style={styles.avImage} source={this.props.avatar} />
          </View>
          <View style={styles.avContent}>
            <Text style={[styles.avTitle, styles.centerTitle]}>
              {this.props.name}
            </Text>
            <Text style={[styles.avSubTitle, styles.centerSubTitle]}>
              {this.props.date}
            </Text>
          </View>
        </View>
      );
    }
    return <Text style={styles.title}>{this.props.contentTitle}</Text>;
  };

  renderRight = () => {
    if (this.props.right === 'notify') {
      return (
        <Notify
          hasNotify={Notifications.newNotification}
          number={Notifications.numberNotification}
        />
      );
      // return <Notify hasNotify={Notifications.newNotification} />;
    }
    if (this.props.right === 'reminder') {
      return <Image style={styles.iconAdd} source={Images.ic_add_remind} />;
    }
    if (this.props.right === 'group') {
      return (
        <View style={{flexDirection: 'row'}}>
          {!this.props.group ? null : (
            <TouchableOpacity
              onPress={this.props.onRightPress}
              style={{paddingHorizontal: getWidth(10)}}>
              <Image style={styles.iconAdd} source={Images.ic_groups} />
            </TouchableOpacity>
          )}
          <TouchableOpacity
            onPress={this.props.onSearch}
            style={{paddingHorizontal: getWidth(10)}}>
            <Image style={styles.iconAdd} source={Images.ic_search_new} />
          </TouchableOpacity>
        </View>
      );
    }
    if (this.props.right === 'options') {
      return <Image style={styles.iconAdd} source={Images.ic_options} />;
    }
    if (this.props.right === 'delete_all') {
      return (
        <View style={styles.chooseAll}>
          <TouchableOpacity
            style={styles.btnChooseAll}
            onPress={() => {
              this.props.chooseAll !== 'undefined' && this.props.chooseAll();
            }}>
            <Text style={styles.textChooseAll}>Chọn tất cả</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btnCloseAll}
            onPress={() => {
              this.props.closeAll !== 'undefined' && this.props.closeAll();
            }}>
            <Image style={styles.iconClose} source={Images.ic_close} />
          </TouchableOpacity>
        </View>
      );
    }
    if (this.props.right === 'filter') {
      return <Image style={styles.iconAdd} source={Images.ic_filter1} />;
    }
    if (this.props.right === 'confirm') {
      return (
        <View style={styles.confirm}>
          <View style={styles.cfRow}>
            <Image style={styles.cfIcon} source={Images.ic_confirm} />
            <Text style={styles.cfText}>Đã xác nhận lúc</Text>
          </View>
          <Text style={styles.cfDate}>{this.props.dateConfirm}</Text>
        </View>
      );
    }
  };

  render() {
    return (
      <View
        style={[
          styles.container,
          this.props.backgroundColor && {
            backgroundColor: this.props.backgroundColor,
          },
        ]}>
        <StatusBar
          barStyle={this.props.barStyle || 'dark-content'}
          backgroundColor={'transparent'}
          translucent={true}
          animated={true}
        />
        <View style={styles.header}>
          <View style={styles.content}>
            <TouchableOpacity
              activeOpacity={1}
              style={[styles.left, this.props.styleLeft]}
              onPress={() => this.onLeftPress()}>
              {this.renderLeft()}
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={1}
              onPress={this.props.onCenterPress}
              style={[styles.center, this.props.styleCenter]}>
              {this.renderCenter()}
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={1}
              style={[styles.right, this.props.styleRight]}
              onPress={() => this.onRightPress()}>
              {this.renderRight()}
            </TouchableOpacity>
          </View>
          {typeof this.props.renderCustom !== 'undefined' &&
            this.props.renderCustom()}
        </View>
      </View>
    );
  }
}

export default function (props) {
  const navigation = useNavigation();
  return <Header {...props} navigation={navigation} />;
}
