import {StyleSheet, Platform} from 'react-native';
import {
  getHeight,
  getWidth,
  fontFamily,
  getStatusBarHeight,
} from '@common/index';

const styles = StyleSheet.create({
  container: {
    marginHorizontal: getHeight(15),
    position: 'relative',
    backgroundColor: 'transparent',
    height: getHeight(60) + getStatusBarHeight(),
  },
  header: {
    position: 'absolute',
    top: getStatusBarHeight() + 10,
    left: 0,
    zIndex: 1,
    width: '100%',
    height: getHeight(45),
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  // left style
  left: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    zIndex: 3,
    justifyContent: 'center',
    paddingRight: getWidth(10),
  },
  back: {
    width: getWidth(14.99),
    height: getHeight(22),
    resizeMode: 'contain',
  },
  ic_logo: {
    width: getWidth(78),
    height: getHeight(48),
    resizeMode: 'contain',
  },
  avatar: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  avWrapImage: {
    width: getHeight(36),
    height: getHeight(36),
    borderRadius: getHeight(36),
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
  avImage: {
    width: getWidth(36),
    height: getHeight(36),
    resizeMode: 'cover',
  },
  avContent: {
    marginLeft: getHeight(12),
  },
  avTitle: {
    fontFamily: fontFamily.f2,
    fontSize: getHeight(17.33),
    color: '#000001',
  },
  avSubTitle: {
    fontFamily: fontFamily.f1,
    fontSize: getHeight(14.67),
    color: '#e96709',
  },
  // center style
  center: {
    flex: 1,
    flexGrow: 1,
    paddingHorizontal: getWidth(40),
  },
  title: {
    textAlign: 'center',
    fontFamily: fontFamily.f2,
    fontSize: getHeight(17.13),
    color: '#000001',
    textAlignVertical: 'center',
  },
  subTitle: {
    fontFamily: fontFamily.f2,
    textAlign: 'center',
    fontSize: getHeight(17.2),
    color: '#000001',
  },
  subMainTitle: {
    fontFamily: fontFamily.f2,
    textAlign: 'center',
    fontSize: getHeight(13.34),
    color: '#e96709',
  },
  centerTitle: {
    fontSize: getHeight(13.33),
  },
  centerSubTitle: {
    fontSize: getHeight(12.67),
    color: '#8d8d8d',
  },
  // right style
  right: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    zIndex: 3,
    paddingLeft: getWidth(10),
    justifyContent: 'center',
  },
  iconAdd: {
    height: getHeight(23),
    width: getWidth(23),
    resizeMode: 'contain',
  },
  // notification
  notifyView: {
    position: 'relative',
  },
  notify: {
    height: getHeight(23),
    width: getWidth(23),
    resizeMode: 'contain',
  },
  hasNotify: {
    height: getHeight(11),
    width: getHeight(11),
    borderRadius: getHeight(11) / 2,
    backgroundColor: '#ff0015',
    position: 'absolute',
    right: Platform.OS === 'ios' ? getWidth(1) : getWidth(2),
    top: getHeight(0.5),
    justifyContent: 'center',
    alignItems: 'center',
  },
  number: {
    fontFamily: fontFamily.f3,
    width: getWidth(20),
    fontSize: getHeight(6),
    color: '#fff',
    textAlign: 'center',
  },
  // confirm
  confirm: {
    backgroundColor: '#f3f3f3',
    borderRadius: getHeight(10),
    paddingVertical: getHeight(3),
    paddingHorizontal: getHeight(10),
  },
  cfRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  cfIcon: {
    height: getHeight(11),
    width: getWidth(11),
    resizeMode: 'contain',
  },
  cfText: {
    fontFamily: fontFamily.f1,
    fontSize: getHeight(10.67),
    color: '#000001',
    marginLeft: getHeight(3),
  },
  cfDate: {
    fontFamily: fontFamily.f1,
    fontSize: getHeight(12.67),
    color: '#8d8d8d',
  },
  // style for delete all
  chooseAll: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  btnChooseAll: {
    borderRadius: getHeight(15),
    overflow: 'hidden',
    backgroundColor: '#0024c3',
    paddingHorizontal: getHeight(12),
    paddingVertical: getHeight(4),
    marginRight: getHeight(10),
  },
  textChooseAll: {
    fontFamily: fontFamily.f1,
    fontSize: getHeight(12.67),
    color: '#fd5df3',
    textAlignVertical: 'center',
    textTransform: 'uppercase',
  },
  btnCloseAll: {
    padding: getHeight(7),
    borderRadius: getHeight(50),
    overflow: 'hidden',
    backgroundColor: '#0024c3',
  },
  iconClose: {
    height: getHeight(11),
    width: getHeight(11),
    resizeMode: 'contain',
    tintColor: '#fd5df3',
  },
});

export default styles;
