
export const Images = {
  background: require('@assets/images/background.png'),
  ic_logo: require('@assets/images/logoAVR.png'),
  ic_back: require('@assets/images/arrow_back.png'),
  ic_mail: require('@assets/images/ic_mail.png'),
  ic_password: require('@assets/images/ic_password.png'),
  ic_fb: require('@assets/images/ic_fb.png'),
  ic_google: require('@assets/images/ic_google.png'),
};
