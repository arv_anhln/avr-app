import React from 'react';
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native';
import {fontFamily, getHeight, getWidth, hasNotch} from '@common/index';

export default function TabBar({
  state,
  descriptors,
  navigation,
  activeTintColor,
  inactiveTintColor,
}) {
  return (
    <View style={styles.container}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            key={index}
            accessibilityRole="button"
            accessibilityStates={isFocused ? ['selected'] : []}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={styles.tab}>
            {options.tabBarIcon({
              focused: isFocused,
              color: isFocused ? activeTintColor : inactiveTintColor,
            })}
            <Text
              style={[
                styles.label,
                {color: isFocused ? activeTintColor : inactiveTintColor},
              ]}>
              {label}
            </Text>
            {isFocused ? <View style={styles.activeIndicator} /> : null}
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const styles = StyleSheet.create({
  label: {
    paddingTop: getHeight(5),
    fontFamily: fontFamily.f1,
    fontSize: getHeight(12),
  },
  container: {
    borderTopWidth: getHeight(0.5),
    borderTopColor: '#b2b2b2',
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    justifyContent: 'space-between',
    paddingHorizontal: getWidth(11),
    paddingBottom: hasNotch() ? getHeight(27) : 0,
  },
  tab: {
    minWidth: getWidth(60),
    alignItems: 'center',
    position: 'relative',
    paddingTop: getHeight(10),
    paddingBottom: getHeight(10),
  },
  // activeIndicator: {
  //   position: 'absolute',
  //   backgroundColor: '#48d2ff',
  //   height: getHeight(2),
  //   width: '100%',
  //   bottom: 0,
  // },
});
